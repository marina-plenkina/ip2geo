<?php

header('Content-Type: application/json; charset=utf-8');

if (empty($_GET['ip']) || ip2long($_GET['ip']) === false) {
    http_response_code(400);
    exit('{}');
}

$filename = sprintf('cache/%s.json', strtr($_GET['ip'], '.', '/'));
$data = [];

if (file_exists($filename) && (time() - filemtime($filename)) / 60 < 30) {
    $data = @json_decode(file_get_contents($filename), true);
}

if (empty($data) && $data = connect_freegeoip($_GET['ip'])) {    
    if(!is_dir(dirname($filename))) {
        mkdir(dirname($filename), 0644, true);
    }
    @file_put_contents($filename, json_encode($data));
} 

if (empty($data)) {
    http_response_code(500);
    exit('{}');
    
} elseif (empty($data['latitude']) && empty($data['longitude']) && empty($data['country']) && empty($data['city'])) {
    http_response_code(404);
    exit('{}');
}

echo json_encode($data);



function connect_freegeoip($ip) {
    $url = sprintf('http://freegeoip.net/json/%s', $ip);
    
    if ($json = @json_decode(file_get_contents($url))) {        
        return array(
            'latitude'  => $json->latitude,
            'longitude' => $json->longitude,
            'country'   => $json->country_name,
            'city'      => $json->city
        );
    }
    
    return false;    
}
