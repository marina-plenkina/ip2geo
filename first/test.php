<?php

class File {
    protected $_cnt = 0;
    
    function __construct() {
        $this->_cnt = 1;
        echo 'open', PHP_EOL;
    }
    
    function __destruct() {
        if(--$this->_cnt === 0) {
            echo 'close', PHP_EOL;
        }
    
    }
    
    function __clone() {
        $this->_cnt = &$this->_cnt;
        $this->_cnt++;
    }    
}

$file  = new File();
$file1 = $file;

$file2 = clone $file;

unset($file1);
