<?php

use Slim\App;

require __DIR__ . '/../vendor/autoload.php';

use Marishik\GeoIp\FreeGeoIpProvider;
use Marishik\Cache\FileCache;

use Tsarkov\GeoIp\LogProvider;
use Tsarkov\GeoIp\CacheProvider;

use Monolog\Logger;

$app = new App();

$container = $app->getContainer();

$container['provider'] = function () use ($container) {
    $provider = new FreeGeoIpProvider();
    return new CacheProvider(new LogProvider($provider, $container->logger), $container->cache);
};

$container['logger'] = function () {
    return new Logger('default', [
        new Monolog\Handler\StreamHandler('../logs/query.log'),
    ]);
};

$container['cache'] = function () {
    return new FileCache(__DIR__ . '/../cache');
};

        $app->get('/ip/{addr}', function ($request, $response, $args) {
            return $this->provider->query($args['addr']);
        });

        $app->run();
