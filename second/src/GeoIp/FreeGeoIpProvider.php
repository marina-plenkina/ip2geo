<?php

namespace Marishik\GeoIp;

use Tsarkov\GeoIp\AbstractProvider;

class FreeGeoIpProvider extends AbstractProvider
{
    public function query(string $addr): string
    {
        $url = sprintf('http://freegeoip.net/json/%s', $addr);
    
        if ($json = @json_decode(file_get_contents($url))) {
            return $json->city;
        }
        return '';
    }
}
