<?php

namespace Marishik\Cache;

use Psr\SimpleCache\CacheInterface;

class FileCache implements CacheInterface
{
    protected $path;
    
    protected function formatKey($key)
    {
        return sprintf("%s/%s.log", $this->path, strtr($key, '.', '/'));
    }

    public function __construct($path)
    {
        $this->path = $path;
    }

    public function get($key, $default = null)
    {
        $filename = $this->formatKey($key);
        return file_get_contents($filename);
    }

    public function set($key, $value, $ttl = null)
    {
        $filename = $this->formatKey($key);
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0644, true);
        }
        @file_put_contents($filename, $value);
    }

    public function delete($key)
    {
        $filename = $this->formatKey($key);
        unlink($filename);
    }

    public function clear()
    {
    }

    public function getMultiple($keys, $default = null)
    {
        $values = array();
        foreach ($keys as $key) {
            $values[] = $this->get($key, $default);
        }
        return $values;
    }

    public function setMultiple($values, $ttl = null)
    {
    }

    public function deleteMultiple($keys)
    {
        foreach ($keys as $key) {
            $this->delete($key);
        }
    }

    public function has($key)
    {
        $filename = $this->formatKey($key);
        return file_exists($filename);
    }
}
